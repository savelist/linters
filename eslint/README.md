# Savelist ESLint Configuration

ESLint rules configuration for Savelist JavaScript projects, extending from Airbnb base rules.

Includes custom configuration for rules:

```
"arrow-parens": ["error", "as-needed"],
"comma-dangle": ["error", "never"],
"consistent-return": ["off"],
"indent": [
  "error",
  2,
  {
    "SwitchCase": 1,
    "MemberExpression": 0
  }
],
"no-param-reassign": [
  "error",
  {
    "props": false
  }
],
"no-plusplus": ["off"],
"no-return-assign": ["off"],
"no-restricted-properties": [
  "error",
  {
    "object": "arguments",
    "property": "callee",
    "message": "arguments.callee is deprecated"
  },
  {
    "property": "__defineGetter__",
    "message": "Please use Object.defineProperty instead."
  },
  {
    "property": "__defineSetter__",
    "message": "Please use Object.defineProperty instead."
  }
],
"no-shadow": ["off"],
"no-underscore-dangle": [
  "error",
  {
    "allow": ["_id"]
  }
],
"no-unused-vars": [
  "error",
  {
    "varsIgnorePattern": "debug"
  }
]
```
